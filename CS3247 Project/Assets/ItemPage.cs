using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemPage : MonoBehaviour
{
    public GameObject weapon_page;
    public GameObject item_page;

    public void ShowPage()
    {
        weapon_page.SetActive(false);
        item_page.SetActive(true);
    }
}
