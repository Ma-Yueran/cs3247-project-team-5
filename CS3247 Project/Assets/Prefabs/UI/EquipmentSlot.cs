using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

/* Sits on all InventorySlots. */

public class EquipmentSlot : MonoBehaviour
{

	public Image icon;

	public Item item;  // Current item in the slot

	// Add item to the slot
	public void AddItem(Item newItem)
	{
		item = newItem;

		icon.sprite = item.icon;
		icon.enabled = true;
	}

	// Clear the slot
	public void ClearSlot()
	{
		item = null;

		icon.sprite = null;
		icon.enabled = false;
	}

	public void OnCursorEnter()
	{
		if (item != null)
		{
			InventoryUI.Instance.DisplayItemInfo(item.name, item.ItemInfo(), transform.position);
		}
	}

	public void OnCursorExit()
	{
		if (item != null)
		{
			InventoryUI.Instance.DestroyItemInfo();
		}
	}
}
