using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class Enhance : MonoBehaviour
{
    public Text Gold;
    public Text Price;
    public GameObject EXP_current;
    public GameObject EXP_enhance;

    private Vector3 scaleChange, positionChange;

    public void enhance()
    {
        scaleChange = new Vector3(EXP_enhance.transform.localScale.x, 0.0f, 0.0f);
        positionChange = new Vector3(EXP_enhance.transform.position.x, 0.0f, 0.0f);
        int value = Convert.ToInt32(Gold.text) - Convert.ToInt32(Price.text);
        if (value > 0)
        {
            Gold.text = value.ToString("000000");
            Debug.Log("a  " + EXP_current.transform.position);
            Debug.Log("b  " + EXP_current.transform.localScale);
            Debug.Log("c  " + EXP_enhance.transform.position);
            Debug.Log("d  " + EXP_enhance.transform.localScale);
            EXP_current.transform.localScale += scaleChange;
            EXP_current.transform.position += scaleChange / 2;
            EXP_enhance.transform.position += scaleChange;

            if (EXP_enhance.transform.position.x + EXP_enhance.transform.localScale.x / 2 > 1.662628f)
            {

                float current_pos_x = EXP_current.transform.position.x;
                float enhance_pos_y = EXP_enhance.transform.position.y;
                float enhance_pos_z = EXP_enhance.transform.position.z;
                float current_scale_x = EXP_current.transform.localScale.x;
                float enhance_scale_y = EXP_enhance.transform.localScale.y;
                float enhance_scale_z = EXP_enhance.transform.localScale.z;
                EXP_enhance.transform.position = new Vector3((current_pos_x + current_scale_x + 1.662628f) / 2.0f, enhance_pos_y, enhance_pos_z);
                // Debug.Log(EXP_enhance.transform.position);
                EXP_enhance.transform.localScale = new Vector3((1.662628f - EXP_enhance.transform.position.x) / 2.0f, enhance_scale_y, enhance_scale_z);
            }
            Debug.Log("aa  " + EXP_current.transform.position);
            Debug.Log("bb  " + EXP_current.transform.localScale);
            Debug.Log("cc  " + EXP_enhance.transform.position);
            Debug.Log("dd  " + EXP_enhance.transform.localScale);
        }
    }
}
