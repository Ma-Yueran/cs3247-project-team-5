using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class DamageReceiver : MonoBehaviour
{
    [HideInInspector]
    public Faction faction;

    public abstract void TakeDamage(AttackInfo attackInfo, int weaponDamage);
}
