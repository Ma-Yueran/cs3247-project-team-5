using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponInventory : Inventory
{
    public LinkedList<Weapon> WeaponKeys;

    public void Add(Weapon weapon) {
        WeaponKeys.AddLast(weapon);
    }

    public void Destroy(Weapon weapon) {
        WeaponKeys.Remove(weapon);
    }
}
