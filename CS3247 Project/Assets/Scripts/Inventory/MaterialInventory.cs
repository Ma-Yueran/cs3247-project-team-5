using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaterialInventory : Inventory
{
    public LinkedList<LootMaterial> MaterialKeys;
    public Dictionary<LootMaterial, int> QuantityMap;

    public void Add(LootMaterial material, int quantity) {

        if (MaterialKeys.Contains(material)) {
            QuantityMap[material] += quantity;
        } else {
            MaterialKeys.AddLast(material);
            QuantityMap[material] = quantity;
        }
        
    }

    public void Destroy(LootMaterial material, int quantity) {
        QuantityMap[material] -= quantity;

        if (QuantityMap[material] == 0) {
            MaterialKeys.Remove(material);
        }
    }

    public int GetQuantity(LootMaterial material) {
        return QuantityMap[material];
    }
}
