using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : MonoBehaviour
{
    public float speed;
    // not real gravity, constant speed
    public float gravity = 0f;
    public float maxLifeTime = 10;
    public TrailRenderer trailRenderer;

    private bool isActive = false;
    private AttackInfo attackInfo;
    private Collider arrowCollider;
    private Faction faction;
    private int damage;

    public void Setup(AttackInfo attackInfo, Faction faction, int damage)
    {
        this.attackInfo = attackInfo;
        this.faction = faction;
        this.damage = damage;

        Destroy(gameObject, maxLifeTime);
    }

    private void Start()
    {
        arrowCollider = GetComponent<Collider>();
    }

    private void Update()
    {
        if (isActive)
        {
            transform.position += speed * transform.forward * Time.deltaTime;
            transform.position -= gravity * transform.up * Time.deltaTime;
        }
    }

    public void Activate(AttackInfo attackInfo)
    {
        isActive = true;
        this.attackInfo = attackInfo;
        trailRenderer.enabled = true;
        arrowCollider.enabled = true;
        Destroy(gameObject, maxLifeTime);
    }

    public void Disable()
    {
        // todo
        Destroy(gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        DamageReceiver damageReceiver = other.GetComponent<DamageReceiver>();
        if (damageReceiver != null && damageReceiver.faction != faction)
        {
            damageReceiver.TakeDamage(attackInfo, damage);
            Disable();
        }
        else if (other.tag == "Environment")
        {
            Disable();
        }
    }
}
