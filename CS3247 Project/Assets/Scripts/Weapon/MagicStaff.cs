using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagicStaff : Weapon
{
    public GameObject spellPrefab;

    private AudioSource source;

    private CharacterAiming characterAiming;
    private Transform ownerTransform;
    private Transform rightHand;

    private void Start()
    {
        source = GetComponent<AudioSource>();
    }

    public override void SetOwner(WeaponManager weaponManager)
    {
        base.SetOwner(weaponManager);
        ownerTransform = weaponManager.transform;
        rightHand = weaponManager.rightHand;
        characterAiming = weaponManager.GetComponent<CharacterAiming>();
    }

    public override void Activate(AttackInfo attackInfo)
    {
        source.PlayOneShot(activateSounds[(int)attackInfo.power]);

        MagicSpell spell = Instantiate(spellPrefab, rightHand.position, Quaternion.identity).GetComponent<MagicSpell>();
        spell.Setup(attackInfo, faction, CalculateWeaponAttack());
        if (characterAiming.target != null)
        {
            spell.transform.LookAt(characterAiming.target.position + Vector3.up * 0.8f);
        }
        else
        {
            spell.transform.LookAt(spell.transform.position + ownerTransform.forward, Vector3.up);
        }
    }
}
