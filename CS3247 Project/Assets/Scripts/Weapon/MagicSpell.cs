using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagicSpell : MonoBehaviour
{
    public float speed;
    public float maxLifeTime = 10f;
    public GameObject explosionPrefab;

    private AttackInfo attackInfo;
    private Faction faction;
    private int damage;

    public void Setup(AttackInfo attackInfo, Faction faction, int damage)
    {
        this.attackInfo = attackInfo;
        this.faction = faction;
        this.damage = damage;

        Destroy(gameObject, maxLifeTime);
    }

    private void Update()
    {
        transform.position += transform.forward * Time.deltaTime * speed;
    }

    private void OnTriggerEnter(Collider other)
    {
        DamageReceiver dr = other.GetComponent<DamageReceiver>();
        if (dr != null && dr.faction != faction)
        {
            Instantiate(explosionPrefab, transform.position, Quaternion.identity);
            dr.TakeDamage(attackInfo, damage);
            Destroy(gameObject);
        }
        else if (other.tag == "Environment")
        {
            Destroy(gameObject);
        }
    }
}
