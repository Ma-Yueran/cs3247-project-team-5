using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Weapon Combination", menuName = "ScriptableObjects/WeaponCombination")]
public class WeaponCombination : ScriptableObject
{
    public GameObject mainWeapon;
    public GameObject secondaryWeapon;
}
