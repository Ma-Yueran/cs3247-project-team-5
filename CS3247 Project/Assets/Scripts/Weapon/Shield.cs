using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shield : Weapon
{
    public override void Activate(AttackInfo attackInfo)
    {
        // do nothing
    }

    public override void SetOwner(WeaponManager weaponManager)
    {
        base.SetOwner(weaponManager);
        weaponManager.EquipBlocker(GetComponent<Blocker>());
    }
}
