using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeWeapon : Weapon
{
    public bool canBlock = false;

    protected Collider weaponCollider;
    protected AudioSource source;

    protected AttackInfo attackInfo;

    private void Awake()
    {
        weaponCollider = GetComponent<Collider>();
        source = GetComponent<AudioSource>();
    }

    public override void Activate(AttackInfo attackInfo)
    {
        this.attackInfo = attackInfo;

        StartCoroutine(EnableWeaponCollider());

        source.PlayOneShot(activateSounds[(int)attackInfo.power]);
    }

    public override void SetOwner(WeaponManager weaponManager)
    {
        base.SetOwner(weaponManager);
        Physics.IgnoreCollision(weaponCollider, weaponManager.GetOwnerCollider());

        if (canBlock)
        {
            weaponManager.EquipBlocker(GetComponent<Blocker>());
        }
    }

    public override void CancelAttack()
    {
        weaponCollider.enabled = false;
    }

    protected IEnumerator EnableWeaponCollider()
    {
        weaponCollider.enabled = true;
        yield return new WaitForSeconds(0.2f);
        weaponCollider.enabled = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        DamageReceiver dr = other.GetComponent<DamageReceiver>();
        if (dr != null)
        {
            if (dr.faction == faction) // hit ally, don't deal damage
            {
                return;
            }
            int weaponDamage = CalculateWeaponAttack(); // todo: calculate rawDamage
            dr.TakeDamage(attackInfo, weaponDamage);   
        }
    }

    private void AddWeaponEXP(int expToAdd)
    {
        // Add EXP to weapon during enhancement up to max EXP points (tentaitvely 10000)
        this.weaponEXP = Mathf.Min((this.weaponEXP + expToAdd), 10000);
    }
}
