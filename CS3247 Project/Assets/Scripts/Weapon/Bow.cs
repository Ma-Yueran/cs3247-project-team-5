using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bow : Weapon
{
    public Transform bowString;
    public GameObject arrowPrefab;
    public Transform arrowLookAt;

    private Transform aimTarget;
    private Transform drawHand;
    private Vector3 stringInitPos;
    private bool isDrawn;
    private Arrow currentArrow;

    private void Start()
    {
        stringInitPos = bowString.localPosition;
    }

    private void Update()
    {
        if (isDrawn)
        {
            bowString.position = drawHand.position;
        }
        else
        {
            bowString.localPosition = stringInitPos;
        }

        if (currentArrow != null)
        {
            currentArrow.transform.LookAt(arrowLookAt);
            currentArrow.transform.position = drawHand.position;
        }
    }

    public override void SetOwner(WeaponManager weaponManager)
    {
        base.SetOwner(weaponManager);

        aimTarget = weaponManager.aimTarget;

        if (weaponHolder == Holder.LeftHand)
        {
            drawHand = weaponManager.rightHand;
        }
        else
        {
            drawHand = weaponManager.leftHand;
        }
    }

    public override void SetUpAttack()
    {
        isDrawn = true;
        currentArrow = Instantiate(arrowPrefab).GetComponent<Arrow>();
    }

    public override void Activate(AttackInfo attackInfo) 
    {
        isDrawn = false;
        currentArrow.transform.LookAt(aimTarget);
        currentArrow.Setup(attackInfo, faction, CalculateWeaponAttack());
        currentArrow.Activate(attackInfo);
        currentArrow = null;
    }

    public override void CancelAttack()
    {
        if (currentArrow != null)
        {
            currentArrow.Disable();
        }
    }
}
