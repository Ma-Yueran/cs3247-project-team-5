using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Holder
{
    LeftHand,
    RightHand,
    Generic
}

public abstract class Weapon : MonoBehaviour
{
    [HideInInspector]
    public Faction faction;

    public CombatStyle combatStyle;
    public RuntimeAnimatorController controller;

    public Vector3 inUsePosition;
    public Vector3 inUseRotation;
    public Holder weaponHolder;
    public AudioClip[] activateSounds;

    // todo: add weapon stats as public variables
    public int weaponPower;
    public float weaponSpeedMultiplier;

    public int weaponEXP;

    public virtual void SetUpAttack()
    {
        // default: do nothing
    }

    public abstract void Activate(AttackInfo attackInfo);

    public virtual void SetOwner(WeaponManager weaponManager)
    {
        faction = weaponManager.faction;

        Transform holder = null;
        if (weaponHolder == Holder.LeftHand)
        {
            holder = weaponManager.leftHand;
        }
        else if (weaponHolder == Holder.RightHand)
        {
            holder = weaponManager.rightHand;
        } else if (weaponHolder == Holder.Generic)
        {
            holder = weaponManager.transform;
        }

        transform.SetParent(holder);
        transform.localPosition = inUsePosition;
        transform.localRotation = Quaternion.Euler(inUseRotation);
    }

    public virtual void CancelAttack()
    {

    }

    protected int CalculateWeaponAttack()
    {
        // Placeholder calculation adding 1/100th of weapon exp for attack boost

        this.weaponPower += (int)Mathf.Floor(this.weaponEXP / 100f);
        int weaponDamage = weaponPower;
        weaponDamage += GetComponentInParent<CharacterStats>().attack.GetValue();
        return weaponDamage;
    }
}
