using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blocker : MonoBehaviour
{
    public AudioClip[] blockSounds;
    public bool isShield = false;

    private AudioSource source;

    private void Start()
    {
        source = GetComponent<AudioSource>();
    }

    public void PlayBlockSound(AttackPower power)
    {
        source.PlayOneShot(blockSounds[(int)power]);
    }

    public void SetOwner(WeaponManager weaponManager)
    {
        if (!isShield)
        {
            return;
        }

        GetComponent<Shield>().SetOwner(weaponManager);
    }
}
