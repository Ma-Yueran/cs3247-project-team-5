using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    public static UIManager instance;

    private List<UIObject> uiObjects;

    private int numOfUI = 0;
    private List<int> activeUIs;

    private void Awake()
    {
        instance = this;
        uiObjects = new List<UIObject>();
        activeUIs = new List<int>();
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    public void RegisterUI(UIObject ui)
    {
        ui.id = numOfUI;
        uiObjects.Add(ui);
        numOfUI++;
    }

    public bool ActivateUI(int id)
    {
        if (!uiObjects[id].forceOpen)
        {
            foreach (int ui in activeUIs)
            {
                if (!uiObjects[ui].allowOtherUI)
                {
                    return false;
                }
            }
        }

        activeUIs.Add(id);

        if (uiObjects[id].requireCursor)
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }

        if (uiObjects[id].pauseGame)
        {
            Time.timeScale = 0;
        }

        return true;
    }

    public void InactivateUI(int id)
    {
        if (activeUIs.Contains(id))
        {
            activeUIs.Remove(id);

            bool stillRequireCursor = false;
            bool stillPauseGame = false;

            foreach (int ui in activeUIs)
            {
                if (uiObjects[ui].requireCursor)
                {
                    stillRequireCursor = true;
                }

                if (uiObjects[ui].pauseGame)
                {
                    stillPauseGame = true;
                }
            }

            if (!stillRequireCursor)
            {
                Cursor.visible = false;
                Cursor.lockState = CursorLockMode.Locked;
            }

            if (stillPauseGame)
            {
                Time.timeScale = 0;
            }
            else 
            {
                Time.timeScale = 1;
            }
        }
    }
}
