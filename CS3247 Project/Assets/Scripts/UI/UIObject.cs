using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIObject : MonoBehaviour
{
    public int id;
    public bool pauseGame = true;
    public bool requireCursor = true;
    public bool allowOtherUI = false;
    public bool forceOpen = false;

    private UIManager uiManager;
    private bool isEnabled;

    protected void Init()
    {
        uiManager = UIManager.instance;
        uiManager.RegisterUI(this);
        gameObject.SetActive(false);
        isEnabled = false;
    }

    private void Start()
    {
        Init();
    }

    public bool IsEnabled()
    {
        return isEnabled;
    }

    public void EnableUI()
    {
        if (uiManager.ActivateUI(id))
        {
            gameObject.SetActive(true);
            Cursor.visible = requireCursor;
            isEnabled = true;
        }
    }

    public void DisableUI()
    {
        gameObject.SetActive(false);
        uiManager.InactivateUI(id);
        isEnabled = false;
    }
}
