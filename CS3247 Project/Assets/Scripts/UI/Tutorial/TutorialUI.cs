using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialUI : MonoBehaviour
{
    public UIObject[] slides;

    private int currentSlide;

    private void Start()
    {
        Invoke("StartTutorial", 0.3f);
    }

    public void StartTutorial()
    {
        foreach (UIObject slide in slides)
        {
            slide.DisableUI();
        }

        currentSlide = 0;
        slides[currentSlide].EnableUI();
    }

    public void NextSlide()
    {
        slides[currentSlide].DisableUI();
        currentSlide++;
        if (currentSlide < slides.Length)
        {
            slides[currentSlide].EnableUI();
        }
    }
}
