using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadScene : MonoBehaviour
{
    [SerializeField]
    private string sceneName;

    public LoadScene(string sceneName)
    {
        this.sceneName = sceneName;
    }

    public void loadScene()
    {
        SceneManager.LoadScene(sceneName);
    }
}
