using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CombatStyle
{
    Melee,
    Bow,
    Magic
}

public abstract class CharacterAction : MonoBehaviour
{
    public CombatStyle combatStyle;

    protected bool canDoAction = true;

    protected Animator animator;
    protected CharacterMotion motion;
    protected CharacterAiming aiming;
    protected WeaponManager weaponManager;

    private void Awake()
    {
        motion = GetComponentInParent<CharacterMotion>();
        aiming = GetComponentInParent<CharacterAiming>();
        weaponManager = GetComponentInParent<WeaponManager>();
    }

    public void EnableCanDoAction()
    {
        canDoAction = true;
    }

    public void DisableAction()
    {
        canDoAction = false;
        motion.DisableCanMove();
        motion.DisableCanRotate();
    }

    public void ResetAction()
    {
        motion.StopLockOn();
        aiming.StopAiming();
        weaponManager.CancelAttack();
        canDoAction = true;
    }

    /// <summary>
    /// Optional actions before attack, such as draw bow string
    /// </summary>
    public void SetUpAttack()
    {
        weaponManager.SetUpAttack();
    }

    public void ActivateAttack(AttackInfo attackInfo)
    {
        weaponManager.ActivateWeapon(attackInfo);
    }

    public void CancelActions()
    {
        // reset triggers
        animator.ResetTrigger("Attack");
        animator.ResetTrigger("Dodge");
    }
}
