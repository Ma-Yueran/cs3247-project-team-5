using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Stat
{
    [SerializeField]
    int baseValue;

    [SerializeField]
    private List<int> modifiers = new List<int>();

    public int GetValue()
    {
        int finalValue = baseValue;
        modifiers.ForEach(x => finalValue += x);
        return finalValue;
    }

    public void AddModifier(int modifier)
    {
        if (modifier != 0)
            modifiers.Add(modifier);
    }

    public void RemoveModifier(int modifier)
    {
        if (modifier != 0)
            modifiers.Remove(modifier);
    }

    public override string ToString()
    {
        int modifierValue = 0;
        modifiers.ForEach(x => modifierValue += x);
        int finalValue = Mathf.Clamp(modifierValue + baseValue, 1, 9999);
        string sign = modifierValue < 0 ? "" : "+";
        return finalValue + " (" + sign + modifierValue + ")";
    }
}
