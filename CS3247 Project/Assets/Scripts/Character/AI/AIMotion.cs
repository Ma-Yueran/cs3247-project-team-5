using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public enum MotionState
{
    Idle,
    Chasing,
    LookAt,
}
public class AIMotion : CharacterMotion
{
    private NavMeshAgent navMeshAgent;
    private Animator animator;
    public Transform currentTarget;

    private MotionState motionState;
    private float timer = 0.3f;

    private void Awake()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
        animator = GetComponentInChildren<Animator>();
    }

    public void ChaseEnemy(Transform enemy, bool isRunning)
    {
        if (enemy == null)
        {
            return;
        }

        currentTarget = enemy;

        motionState = MotionState.Chasing;

        navMeshAgent.enabled = true;
        if (!isRunning)
        {
            navMeshAgent.speed = walkSpeed;
        }
        else
        {
            navMeshAgent.speed = runSpeed;
        }

        animator.SetBool("Is Moving", true);
        animator.SetFloat("Motion V", 1);
        animator.SetFloat("Motion H", 0);
        animator.SetBool("Is Running", isRunning);
    }

    public void Idle()
    {
        motionState = MotionState.Idle;

        navMeshAgent.enabled = false;

        animator.SetBool("Is Moving", false);
        animator.SetFloat("Motion V", 0);
        animator.SetFloat("Motion H", 0);
    }

    public override void LookAtTarget(Transform target)
    {
        base.LookAtTarget(target);
        motionState = MotionState.LookAt;
    }

    public void StartDodge()
    {
        motionState = MotionState.Idle;
        transform.Rotate(Vector3.up, 180);
    }

    public override void DisableCanMove()
    {
        motionState = MotionState.Idle;
    }

    public override void ResetTargets()
    {
        base.ResetTargets();
        Idle();
    }

    private void Update()
    {
        if (motionState == MotionState.Idle)
        {
            return;
        } 
        else if (motionState == MotionState.Chasing)
        {
            if (timer > 0)
            {
                timer -= Time.deltaTime;
            }
            else
            {
                timer += 0.3f;
                navMeshAgent.SetDestination(currentTarget.position);
            }
        } 
        else if (motionState == MotionState.LookAt)
        {
            if (lookAtTarget == null)
            {
                return;
            }

            Vector3 targetDirection = lookAtTarget.position - transform.position;
            targetDirection.y = 0;
            Quaternion lookRotation = Quaternion.LookRotation(targetDirection);
            float rotateSpeed = Mathf.Clamp(this.rotateSpeed * Time.deltaTime, 0.01f, 0.9f);
            transform.rotation = Quaternion.Lerp(transform.rotation, lookRotation, rotateSpeed);
        }
    }
}
