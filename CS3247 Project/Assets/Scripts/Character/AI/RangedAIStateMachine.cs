using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangedAIStateMachine : CharacterManager
{
    public float shootRate;
    public float shootRange;
    public bool canMove = true;

    private StateMachine stateMachine;

    private void Awake()
    {
        stateMachine = new StateMachine();
        
        EnemyDetector targetDetector = GetComponent<EnemyDetector>();
        AIMotion aIMotion = GetComponent<AIMotion>();
        AIAction aIAction = GetComponentInChildren<AIAction>();
        AIAiming aIAiming = GetComponent<AIAiming>();
        CharacterDamageReceiver damageReceiver = GetComponent<CharacterDamageReceiver>();

        Idle idle = new Idle(aIMotion);
        MoveToTarget moveToTarget = new MoveToTarget(aIMotion, targetDetector, 2, false);
        Shoot shoot = new Shoot(aIAction, aIMotion, targetDetector, shootRate);
        Hit hit = new Hit(0.8f, aIMotion, targetDetector);

        if (canMove)
        {
            // any
            stateMachine.AddAnyTransition(idle, () => !targetDetector.hasTarget());
            stateMachine.AddAnyTransition(moveToTarget, () => damageReceiver.IsHit() && !damageReceiver.IsInterupted());
            stateMachine.AddAnyTransition(hit, damageReceiver.IsInterupted, true);

            // idle
            stateMachine.AddTransition(idle, moveToTarget, () => targetDetector.hasTarget());

            // moveToTarget
            stateMachine.AddTransition(moveToTarget, shoot, () => targetDetector.WithinRange(shootRange) && aIAiming.CanHitTarget(targetDetector.GetTarget(), shootRange));

            // shoot
            stateMachine.AddTransition(shoot, idle, () => !targetDetector.WithinRange(shootRange + 4));
            stateMachine.AddTransition(shoot, moveToTarget, () => aIAiming.CanHitTarget(targetDetector.GetTarget(), shootRange + 4));
        }
        else
        {
            // idle
            stateMachine.AddTransition(idle, shoot, () => aIAiming.CanHitTarget(targetDetector.GetTarget(), shootRange));

            // shoot
            stateMachine.AddTransition(shoot, idle, () => !aIAiming.CanHitTarget(targetDetector.GetTarget(), shootRange));
        }

        // initial state
        stateMachine.SetState(idle);
    }

    private void Update()
    {
        stateMachine.Tick();
    }
}
