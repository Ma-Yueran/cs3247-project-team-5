using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIAiming : CharacterAiming
{
    public float distToHeight = 30;

    private EnemyDetector targetDetector;

    private void Start()
    {
        targetDetector = GetComponent<EnemyDetector>();
    }

    public override void Aim()
    {
        Transform aimTarget = targetDetector.GetTarget();
        if (aimTarget == null)
        {
            return;
        }

        target.position = aimTarget.position + Vector3.up * 0.8f;
        float sqrDist = Vector3.SqrMagnitude(target.position - transform.position);
        target.position += Vector3.up * sqrDist / (distToHeight * distToHeight);

        spineRotation.LookAt(target);
        rig.weight = 1;
    }

    public override void StopAiming()
    {
        if (rig != null)
        {
            rig.weight = 0;
        }
    }

    public bool CanHitTarget(Transform target, float maxDistance)
    {
        if (this.target == null)
        {
            this.target = target;
        }

        Vector3 direction = target.position - transform.position;
        direction.Normalize();
        
        RaycastHit hit;
        bool hasHit = Physics.Raycast(transform.position + Vector3.up * 0.8f, direction, out hit, maxDistance, layerMask);
        if (!hasHit)
        {
            return false;
        }

        DamageReceiver dr = hit.collider.GetComponent<DamageReceiver>();
        return dr != null && dr.faction != faction;
    }
}
