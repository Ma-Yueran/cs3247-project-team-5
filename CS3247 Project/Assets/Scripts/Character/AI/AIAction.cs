using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIAction : CharacterAction
{
    private CharacterDamageReceiver damageReceiver;

    private bool isAiming = false;
    public float aimingTime = 2f;
    private float aimingTimer = 0;

    private void Start()
    {
        animator = GetComponent<Animator>();
        damageReceiver = GetComponentInParent<CharacterDamageReceiver>();
    }

    private void Update()
    {
        if (isAiming)
        {
            aimingTimer -= Time.deltaTime;
        }
    }

    public bool Attack(int attackPattern=-1)
    {
        if (canDoAction)
        {
            if (attackPattern != -1)
            {
                animator.SetInteger("Attack Pattern", attackPattern);
            }

            animator.SetTrigger("Attack");
            canDoAction = false;
            return true;
        }
        else
        {
            return false;
        }
    }

    public void StartBlock()
    {
        animator.SetBool("Is Blocking", true);
        damageReceiver.isBlocking = true;
    }

    public void StopBlock()
    {
        animator.SetBool("Is Blocking", false);
        damageReceiver.isBlocking = false;
    }

    public bool CanDoAction()
    {
        return canDoAction;
    }

    public bool Dodge()
    {
        if (canDoAction)
        {
            animator.SetTrigger("Dodge");
            canDoAction = false;
            return true;
        } else
        {
            return false;
        }
    }

    public bool ShootArrow()
    {
        if (!canDoAction)
        {
            animator.SetBool("Can Draw String", false);
            aiming.StopAiming();
            // motion.StopLockOn();
            return false;
        }

        animator.SetBool("Can Draw String", true);

        if (!isAiming)
        {
            animator.SetBool("Draw String", true);
            motion.SetLockOn(aiming.target);
            isAiming = true;
            aimingTimer = aimingTime;
        }
        else if (aimingTimer < -1)
        {
            aiming.StopAiming();
            isAiming = false;
            return true;
        }
        else if (aimingTimer < 0)
        {
            animator.SetBool("Draw String", false);
        }
        else
        {
            aiming.Aim();
        }

        return false;
    }
}
