using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AllyStateMachine : CharacterManager
{
    private StateMachine stateMachine;
    [SerializeField] 
    private float restChance = 0.6f;
    [SerializeField] 
    private float dodgeChance = 0.1f;
    [SerializeField] 
    private float blockChance = 0.3f;
    [SerializeField]
    private float attackRange = 2f;
    [SerializeField]
    private bool canRun = false;
    [SerializeField]
    private int numOfAttackPattern = -1;
    
    // Start is called before the first frame update
    void Awake()
    {
        EnemyDetector enemyDetector = GetComponent<EnemyDetector>();
        AIMotion aIMotion = GetComponent<AIMotion>();
        AIAction aIAction = GetComponentInChildren<AIAction>();
        AIAiming aIAiming = GetComponent<AIAiming>();
        CharacterDamageReceiver damageReceiver = GetComponent<CharacterDamageReceiver>();

        RandomGenerator afterAttack = new RandomGenerator(new Choice(State.Rest, restChance), 
                                                          new Choice(State.Dodge, dodgeChance), 
                                                          new Choice(State.Block, blockChance));

        stateMachine = new StateMachine();

        Idle allyIdle = new Idle(aIMotion);
        MoveToTarget moveToTarget = new MoveToTarget(aIMotion, enemyDetector, attackRange, canRun);
        Attack attack = new Attack(aIAction, enemyDetector, aIMotion, aIAiming, numOfAttackPattern);
        Rest rest = new Rest(aIMotion, enemyDetector);
        Block block = new Block(aIAction, aIMotion, enemyDetector);
        Dodge dodge = new Dodge(aIAction, aIMotion);
        Hit hit = new Hit(0.8f, aIMotion, enemyDetector);

        At(allyIdle, moveToTarget, () => enemyDetector.hasTarget());

        At(moveToTarget, attack, () => moveToTarget.StateEnded() && aIAiming.CanHitTarget(enemyDetector.GetTarget(), attackRange + 1));

        At(attack, hit, () => damageReceiver.IsInterupted());
        At(attack, rest, () => attack.StateEnded() && afterAttack.RollState() == State.Rest);
        At(attack, dodge, () => attack.StateEnded() && afterAttack.RollState() == State.Dodge);
        At(attack, block, () => attack.StateEnded() && afterAttack.RollState() == State.Block);

        At(rest, moveToTarget, () => rest.StateEnded());

        At(block, moveToTarget, () => block.StateEnded());

        At(dodge, moveToTarget, () => dodge.StateEnded());

        At(hit, moveToTarget, () => hit.StateEnded());

        stateMachine.AddAnyTransition(allyIdle, () => !enemyDetector.hasTarget());
        

        void At(IState from, IState to, Func<bool> condition)
        {
            stateMachine.AddTransition(from, to, condition);
        }

        stateMachine.SetState(allyIdle);
        
    }


    void Update()
    {
        stateMachine.Tick();
    }
}
