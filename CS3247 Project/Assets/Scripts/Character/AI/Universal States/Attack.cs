using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attack : IState
{
    private AIAction aIAction;
    private AIMotion aIMotion;
    private AIAiming aIAiming;
    private EnemyDetector enemyDetector;
    private int numOfAttackPattern;
    private int attackNumber;
    private int attackCount;
    private int attackPattern = -1;

    public Attack(AIAction aIAction, EnemyDetector enemyDetector, AIMotion aIMotion, AIAiming aIAiming, int numOfAttackPattern=-1)
    {
        this.aIAction = aIAction;
        this.aIMotion = aIMotion;
        this.enemyDetector = enemyDetector;
        this.aIAiming = aIAiming;
        this.numOfAttackPattern = numOfAttackPattern;
    }
    public override void OnEnter()
    {
        attackNumber = Random.Range(1, 4);
        attackCount = 0;
        if (numOfAttackPattern != -1)
        {
            attackPattern = Random.Range(0, numOfAttackPattern);
            attackNumber = 1;
        }
        Transform target = enemyDetector.GetTarget();
        aIMotion.LookAtTarget(target);
        aIAiming.target = target;
    }

    public override void OnExit()
    {

    }

    public override void Tick()
    {
        if(attackCount < attackNumber && aIAction.Attack(attackPattern))
        {
            attackCount++;
        }
    }

    public override bool StateEnded()
    {
        return attackCount == attackNumber && aIAction.CanDoAction();
    }

}
