using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hit : IState
{
    private float timer;
    private float stunTime;
    private AIMotion motion;
    private EnemyDetector detector;

    public Hit(float stunTime, AIMotion motion, EnemyDetector detector)
    {
        this.stunTime = stunTime;
        this.motion = motion;
        this.detector = detector;
    }

    public override void OnEnter()
    {
        timer = stunTime;
        motion.LookAtTarget(detector.GetTarget());
    }

    public override void Tick()
    {
        timer -= Time.deltaTime;
    }

    public override bool StateEnded()
    {
        return timer < 0;
    }
}
