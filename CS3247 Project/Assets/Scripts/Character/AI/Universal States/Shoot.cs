using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot : IState
{
    private AIAction action;
    private AIMotion motion;
    private EnemyDetector detector;
    private float shootRate;
    private float timer;

    public Shoot(AIAction action, AIMotion motion, EnemyDetector detector, float shootRate)
    {
        this.action = action;
        this.motion = motion;
        this.detector = detector;
        this.shootRate = shootRate;
    }

    public override void OnEnter()
    {
        motion.LookAtTarget(detector.GetTarget());
        timer = 0;
    }

    public override void Tick()
    {
        timer -= Time.deltaTime;
        if (timer > 0)
        {
            return;
        }
            
        if (action.ShootArrow())
        {
            timer = shootRate;
        }
    }
}
