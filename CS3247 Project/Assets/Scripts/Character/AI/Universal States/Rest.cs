using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rest : IState
{
    private float timer;
    private float minTime;
    private float maxTime;
    private AIMotion motion;
    private EnemyDetector detector;

    public Rest(AIMotion motion, EnemyDetector detector, float minTime=2, float maxTime=3)
    {
        this.motion = motion;
        this.detector = detector;
        this.minTime = minTime;
        this.maxTime = maxTime;
    }

    public override void OnEnter()
    {
        timer = Random.Range(minTime, maxTime);
        motion.LookAtTarget(detector.GetTarget());
    }

    public override void Tick()
    {
        timer -= Time.deltaTime;
    }

    public override bool StateEnded()
    {
        return timer <= 0;
    }
}
