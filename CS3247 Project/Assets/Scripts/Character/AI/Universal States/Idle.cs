using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Idle : IState
{
    private AIMotion motion;

    public Idle(AIMotion motion)
    {
        this.motion = motion;
    }

    public override void Tick()
    {
        motion.Idle();
    }
}
