using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MoveToTarget : IState
{
    private AIMotion aIMotion;
    private EnemyDetector enemyDetector;
    private Transform target;
    private float stopDistance;
    private bool isRunning;

    public MoveToTarget(AIMotion aIMotion, EnemyDetector enemyDetector, float stopDistance, bool isRunning)
    {
        this.aIMotion = aIMotion;
        this.enemyDetector = enemyDetector;
        this.stopDistance = stopDistance;
        this.isRunning = isRunning;
    }

    public override void Tick()
    {
        target = enemyDetector.GetTarget();
        aIMotion.ChaseEnemy(target, isRunning);
    }

    public override void OnExit()
    {
        aIMotion.Idle();
    }

    public override bool StateEnded()
    {
        return Vector3.SqrMagnitude(aIMotion.transform.position - target.position) < stopDistance * stopDistance;
    }
}
