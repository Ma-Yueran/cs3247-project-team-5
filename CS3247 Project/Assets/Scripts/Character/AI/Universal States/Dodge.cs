using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dodge : IState
{
    private AIAction aiAction;
    private AIMotion aiMotion;
    bool hasDodged;
    
    public Dodge(AIAction aiAction, AIMotion aiMotion)
    {
        this.aiAction = aiAction;
        this.aiMotion = aiMotion;
    }
    public override void OnEnter()
    {
        hasDodged = false;
        aiMotion.StartDodge();
    }

    public override void OnExit()
    {
    }

    public override void Tick()
    {
        if (!hasDodged && aiAction.Dodge())
        {
            hasDodged = true;
        }
    }

    public override bool StateEnded()
    {
        return aiAction.CanDoAction() && hasDodged;
    }


}
