using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Block : IState
{
    private AIAction aiAction;
    private AIMotion aiMotion;
    private EnemyDetector detector;
    private float blockTime;

    public Block(AIAction aiAction, AIMotion aiMotion, EnemyDetector detector)
    {
        this.aiAction = aiAction;
        this.aiMotion = aiMotion;
        this.detector = detector;
    }
    public override void OnEnter()
    {
        blockTime = Random.Range(3, 5);
        aiAction.StartBlock();
    }

    public override void OnExit()
    {
        aiAction.StopBlock();
    }

    public override void Tick()
    {
        blockTime -= Time.deltaTime;
        aiMotion.LookAtTarget(detector.GetTarget());
    }

    public override bool StateEnded()
    {
        return blockTime < 0;
    }
}
