using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDetector : TargetDetector
{
    private LevelManager levelManager;

    public void Start()
    {
        levelManager = LevelManager.instance;
    }

    public Transform GetTarget()
    {
        Transform closest = null;
        float closestDistance = Mathf.Infinity;
        foreach (Transform enemy in levelManager.GetTargets(targetFaction))
        {
            float distance = Vector3.SqrMagnitude(transform.position - enemy.position);
            if (distance < closestDistance)
            {
                closest = enemy;
                closestDistance = distance;
            }
        }
        return closest;
    }

    public bool hasTarget()
    {
        return levelManager.GetTargets(targetFaction).Count > 0;
    }

    public bool WithinRange(float range)
    {
        Transform target = GetTarget();

        if (target == null)
        {
            return false;
        }

        return Vector3.SqrMagnitude(transform.position - GetTarget().position) < range * range;
    }
}
