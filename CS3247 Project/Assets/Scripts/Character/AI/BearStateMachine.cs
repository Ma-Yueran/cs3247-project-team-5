using System;
using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public class BearStateMachine : CharacterManager
{
    public float attackRange;

    private StateMachine stateMachine;
    private EnemyDetector targetDetector;
    private AIMotion aiMotion;
    private AIAction aiAction;

    void Start()
    {
        Init();

        stateMachine = new StateMachine();
        targetDetector = GetComponentInChildren<EnemyDetector>();
        aiMotion = GetComponent<AIMotion>();
        aiAction = GetComponentInChildren<AIAction>();

        Idle idle = new Idle(aiMotion);
        MoveToTarget moveToTarget = new MoveToTarget(aiMotion, targetDetector, attackRange, false);
        Attack attack = new Attack(aiAction, targetDetector, aiMotion, null);
        Rest rest = new Rest(aiMotion, targetDetector);

        // any
        stateMachine.AddAnyTransition(idle, () => !targetDetector.hasTarget());

        // idle
        At(idle, moveToTarget, targetDetector.hasTarget);

        // moveToTarget
        At(moveToTarget, attack, () => targetDetector.WithinRange(attackRange));

        // attack
        At(attack, rest, attack.StateEnded);

        // rest
        At(rest, moveToTarget, rest.StateEnded);

        void At(IState from, IState to, Func<bool> condition)
        {
            stateMachine.AddTransition(from, to, condition);
        }

        stateMachine.SetState(idle);

    }

    void Update()
    {
        stateMachine.Tick();
    }
}
