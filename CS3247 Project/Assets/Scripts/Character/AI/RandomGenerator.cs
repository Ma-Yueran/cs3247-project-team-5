using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum State
{
    Null,
    Rest,
    Dodge,
    Block
}

public class Choice
{
    public State State { get; }
    public float Chance { get; }

    public Choice(State state, float chance)
    {
        State = state;
        Chance = chance;
    }
}

public class RandomGenerator 
{
    private Choice[] choices;

    public RandomGenerator(params Choice[] choices) 
    {
        this.choices = choices;
    }

    public State RollState()
    {
        float rand = Random.Range(0f, 1f);

        foreach (Choice c in choices)
        {
            if (rand < c.Chance)
            {
                return c.State;
            }

            rand -= c.Chance;
        }

        return State.Null;
    }
}
