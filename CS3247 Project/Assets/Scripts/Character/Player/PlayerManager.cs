using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : CharacterManager
{
    private PlayerInputs playerInputs;
    private PlayerMotion playerMotion;
    private PlayerAction playerAction;

    public GameOver gameOverScreen;
    public Vector3 initPos;

    private void Start()
    {
        playerInputs = GetComponent<PlayerInputs>();
        playerMotion = GetComponent<PlayerMotion>();
        playerAction = GetComponentInChildren<PlayerAction>();
        Init();
        //Cursor.visible = false;
    }

    private void Update()
    {
        playerInputs.HandlePlayerInputs();
        playerMotion.HandleMotion();
        playerAction.HandleAction();
        CheckPlayerPosition();
    }

    private void LateUpdate()
    {
        playerInputs.ResetFlags();
    }

    public override void DisableCharacter() {
        base.DisableCharacter();
        gameOverScreen.TriggerLoseScreen();
    }

    private void CheckPlayerPosition()
    {
        if (transform.position.y < -3)
        {
            transform.position = initPos;
        }
    }
}
