using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations.Rigging;

public class PlayerAiming : CharacterAiming
{
    public CameraSetting normalSetting;
    public CameraSetting aimingSetting;
    public GameObject crosshair;

    private Animator animator;
    private ThirdPersonCamera playerCamera;
    private Transform cameraTransform;
    private float maxDistance = 200;
    private bool isAiming = false;

    private void Start()
    {
        animator = GetComponentInChildren<Animator>();
        playerCamera = ThirdPersonCamera.instance;
        cameraTransform = ThirdPersonCamera.instance.cameraTransform;
        target.SetParent(null);
    }

    private void Update()
    {
        if (isAiming)
        {
            rig.weight = 1;
        }
        else
        {
            rig.weight = 0;
        }
    }

    public override void Aim()
    {
        animator.SetBool("Can Draw String", true);

        if (!isAiming)
        {
            playerCamera.ChangeSetting(aimingSetting);
            crosshair.SetActive(true);
        }

        isAiming = true;

        Ray ray = new Ray(cameraTransform.position, cameraTransform.forward);
        RaycastHit hit;
        bool intersect = Physics.Raycast(ray, out hit, maxDistance, layerMask);

        if (intersect)
        {
            target.position = hit.point;
        }
        else
        {
            target.position = ray.GetPoint(maxDistance);
        }

        spineRotation.LookAt(target);
    }

    public override void StopAiming()
    {
        animator.SetBool("Can Draw String", false);
        isAiming = false;
        playerCamera.ChangeSetting(normalSetting);
        crosshair.SetActive(false);
    }
}
