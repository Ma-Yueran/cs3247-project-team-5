using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTargetDetector : TargetDetector
{
    private LevelManager levelManager;

    private Transform playerTransform;

    private void Start()
    {
        playerTransform = transform;
        levelManager = LevelManager.instance;
    }

    public Transform GetTarget()
    {
        if (levelManager == null)
        {
            return null;
        }

        float shortestDist = float.MaxValue;
        Transform closest = null;

        foreach (Transform enemy in levelManager.GetTargets(targetFaction))
        {
            float dist = Vector3.SqrMagnitude(playerTransform.position - enemy.position);
            if (dist < shortestDist)
            {
                closest = enemy;
                shortestDist = dist;
            }
        }

        return closest;
    }
}
