using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAction : CharacterAction
{
    private PlayerInputs playerInputs;
    private CharacterDamageReceiver damageReceiver;
    private PlayerTargetDetector targetDetector;

    private float dodgeCoolDown = 0.8f;
    private float dodgeTimer;

    private void Start()
    {
        animator = GetComponent<Animator>();
        playerInputs = GetComponentInParent<PlayerInputs>();
        damageReceiver = GetComponentInParent<CharacterDamageReceiver>();
        targetDetector = GetComponentInParent<PlayerTargetDetector>();
    }

    public void HandleAction()
    {
        HandleDodge();

        if (combatStyle == CombatStyle.Melee)
        {
            HandleMeleeAttack();
            HandleBlock();
        }
        else if (combatStyle == CombatStyle.Bow)
        {
            HandleShootArrow();
        }
        else if (combatStyle == CombatStyle.Magic)
        {
            HandleMagicAttack();
        }

        HandleSwapWeapon();
    }

    private void HandleMeleeAttack()
    {
        if (!canDoAction)
        {
            return;
        }

        if (playerInputs.attackFlag)
        {
            motion.DisableCanMove();
            motion.LookAtTarget(targetDetector.GetTarget());
            animator.SetTrigger("Attack");
            canDoAction = false;
        }
    }

    private void HandleDodge()
    {
        dodgeTimer -= Time.deltaTime;

        if (playerInputs.dodgeFlag && dodgeTimer < 0)
        {
            damageReceiver.SetInvincible(0.5f);

            dodgeTimer = dodgeCoolDown;

            CancelActions();
            ResetAction();
            motion.ResetMotion();

            motion.DisableCanMove();
            animator.SetTrigger("Dodge");
            canDoAction = false;
        }
    }

    private void HandleBlock()
    {
        if (!canDoAction)
        {
            animator.SetBool("Is Blocking", false);
            damageReceiver.isBlocking = false;
            motion.StopLockOn();
            return;
        }

        animator.SetBool("Is Blocking", playerInputs.isBlocking);
        damageReceiver.isBlocking = playerInputs.isBlocking;

        Transform lockOn = targetDetector.GetTarget();

        if (playerInputs.isBlocking)
        {
            motion.SetLockOn(lockOn);
        }
        else
        {
            motion.StopLockOn();
        }
    }

    private void HandleShootArrow()
    {
        if (!canDoAction)
        {
            aiming.StopAiming();
            motion.StopLockOn();
            return;
        }

        animator.SetBool("Draw String", playerInputs.isDrawingString);
        if (playerInputs.isDrawingString)
        {
            aiming.Aim();
            motion.SetLockOn(aiming.target);
        }
    }

    private void HandleMagicAttack()
    {
        if (!canDoAction)
        {
            return;
        }

        if (playerInputs.attackFlag)
        {
            motion.DisableCanMove();
            Transform target = targetDetector.GetTarget();
            motion.LookAtTarget(target);
            if (target != null) 
            {
                aiming.target.position = target.position;
            }
            else
            {
                aiming.target.position = transform.position + transform.forward * 10f;
            }
            animator.SetTrigger("Attack");
            canDoAction = false;
        }
    }

    private void HandleSwapWeapon()
    {
        if (!canDoAction || playerInputs.isDrawingString) return;

        if (playerInputs.swapWeaponFlag)
        {
            weaponManager.SwapWeapon();
        }
    }
}
