using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PlayerInputs : MonoBehaviour
{
    [HideInInspector]
    public float inputH;
    [HideInInspector]
    public float inputV;
    [HideInInspector]
    public bool isMoving;
    [HideInInspector]
    public bool isRunning = false;
    [HideInInspector]
    public bool isBlocking = false;
    [HideInInspector]
    public bool isDrawingString = false;

    [HideInInspector]
    public bool attackFlag;
    [HideInInspector]
    public bool dodgeFlag;
    [HideInInspector]
    public bool swapWeaponFlag;

    private float maxPressTime = 0.3f;
    private float dodgeTimer;

    public void HandlePlayerInputs()
    {
        HandleMotion();
        HandleAttack();
        HandleDodge();
        HandleBlock();
        HandleShootArrow();
        HandleSwapWeapon();
    }

    public void ResetFlags()
    {
        attackFlag = false;
        dodgeFlag = false;
        swapWeaponFlag = false;
    }

    private void HandleMotion()
    {
        inputH = Input.GetAxis("Horizontal");
        inputV = Input.GetAxis("Vertical");
        isMoving = inputH != 0 || inputV != 0;
        isRunning = Input.GetKey(KeyCode.LeftShift);
    }

    private void HandleAttack()
    {
        if (Input.GetMouseButtonDown(0) && !EventSystem.current.IsPointerOverGameObject())
        {
            attackFlag = true;
        }
    }

    private void HandleDodge()
    {
        dodgeTimer += Time.deltaTime;

        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            dodgeTimer = 0;
        }
        else if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            if (dodgeTimer < maxPressTime)
            {
                dodgeFlag = true;
            }
        }
    }

    private void HandleBlock()
    {
        isBlocking = Input.GetMouseButton(1);
    }

    private void HandleShootArrow()
    {
        isDrawingString = Input.GetMouseButton(0);
    }

    private void HandleSwapWeapon()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            swapWeaponFlag = true;
        }
    }
}
