using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMotion : CharacterMotion
{
    private Transform myTransform;
    private Animator animator;

    private PlayerInputs playerInputs;
    private ThirdPersonCamera thirdPersonCamera;

    private float currentSpeed = 0;

    private void Start()
    {
        myTransform = GetComponent<PlayerManager>().transform;
        playerInputs = GetComponent<PlayerInputs>();
        animator = GetComponentInChildren<Animator>();
        thirdPersonCamera = ThirdPersonCamera.instance;
    }

    public void HandleMotion()
    {
        if (canMove)
        {
            if (isLockOn)
            {
                HandleLockOnMovement();
            }
            else
            {
                HandleNormalMovement();
            }
        }

        if (canRotate)
        {
            if (isLockOn)
            {
                HandleLockOnRotation();
            }
            else
            {
                HandleNormalRotation();
            }
        }
        else if (lookAt)
        {
            HandleLookAt();
        }
    }

    private void HandleNormalMovement()
    {
        animator.SetBool("Is Moving", playerInputs.isMoving);
        animator.SetFloat("Motion H", 0);

        if (!playerInputs.isMoving)
        {
            animator.SetFloat("Motion V", 0, 0.1f, Time.deltaTime);
            return;
        }

        animator.SetFloat("Motion V", 1, 0.1f, Time.deltaTime);
        
        animator.SetBool("Is Running", playerInputs.isRunning);

        if (!playerInputs.isMoving)
        {
            return;
        }

        if (!playerInputs.isRunning)
        {
            currentSpeed = Mathf.Lerp(currentSpeed, walkSpeed, 0.1f);
        }
        else
        {
            currentSpeed = Mathf.Lerp(currentSpeed, runSpeed, 0.1f);
        }

        transform.position += Time.deltaTime * currentSpeed * transform.forward;
    }

    private void HandleNormalRotation()
    {
        if (!playerInputs.isMoving)
        {
            return;
        }

        float rotateSpeed = Mathf.Clamp(this.rotateSpeed * Time.deltaTime, 0.01f, 0.9f);
        Vector3 targetDirection = GetTargetDirection();
        Quaternion targetRotation = Quaternion.LookRotation(targetDirection);
        myTransform.rotation = Quaternion.Lerp(myTransform.rotation, targetRotation, rotateSpeed);
    }

    private void HandleLockOnMovement()
    {
        HandleLockOnRotation();

        animator.SetBool("Is Running", false);

        if (!playerInputs.isMoving)
        {
            animator.SetFloat("Motion H", 0, 0.1f, Time.deltaTime);
            animator.SetFloat("Motion V", 0, 0.1f, Time.deltaTime);
            return;
        }

        Vector3 targetDirection = GetTargetDirection();
        float angle = Vector3.SignedAngle(myTransform.forward, targetDirection, myTransform.up) * Mathf.Deg2Rad;
        float motionH = Mathf.Sin(angle);
        float motionV = Mathf.Cos(angle);

        animator.SetFloat("Motion H", motionH);
        animator.SetFloat("Motion V", motionV);

        currentSpeed = walkSpeed;
        transform.position += Time.deltaTime * currentSpeed * targetDirection;
    }

    private void HandleLockOnRotation()
    {
        float rotateSpeed = Mathf.Clamp(this.rotateSpeed * Time.deltaTime, 0.01f, 0.9f);
        Vector3 targetDirection = currentLockOn.position - myTransform.position;
        targetDirection.y = 0;
        Quaternion targetRotation = Quaternion.LookRotation(targetDirection);
        myTransform.rotation = Quaternion.Lerp(myTransform.rotation, targetRotation, rotateSpeed);
    }

    private void HandleLookAt()
    {
        currentSpeed = 0;
        Vector3 targetDirection = lookAtTarget.position - transform.position;
        targetDirection.y = 0;
        Quaternion lookRotation = Quaternion.LookRotation(targetDirection);
        transform.rotation = Quaternion.Lerp(transform.rotation, lookRotation, 0.1f);
    }

    private Vector3 GetTargetDirection()
    {
        Vector3 targetDirection = thirdPersonCamera.cameraTransform.forward * playerInputs.inputV;
        targetDirection += thirdPersonCamera.cameraTransform.right * playerInputs.inputH;
        targetDirection.y = 0;
        targetDirection.Normalize();

        return targetDirection;
    }
}
