using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerStats : CharacterStats
{
    public static PlayerStats Instance;

    public TMP_Text Health;
    public TMP_Text Gold;
    public TMP_Text Attack;
    public TMP_Text Defence;

    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(this);
        } else
        {
            Instance = this;
        }
    }

    public void ChangeEquipment(Item currItem, Item newItem)
    {
        base.attack.AddModifier(newItem.Attack);
        base.defence.AddModifier(newItem.Defence);
        if (currItem != null)
        {
            base.attack.RemoveModifier(currItem.Attack);
            base.defence.RemoveModifier(currItem.Defence);
        }
        UpdateStatUI();
    }

    public void Unequip(Item item)
    {
        base.attack.RemoveModifier(item.Attack);
        base.defence.RemoveModifier(item.Defence);
        UpdateStatUI();
    }

    public void UpdateStatUI()
    {
        Health.text = health + "/" + maxHealth;
        Gold.text = GoldManager.Instance.gold.ToString();
        Attack.text = attack.ToString();
        Defence.text = defence.ToString();
    }

    public override void TakeDamage(int damage)
    {
        base.TakeDamage(damage);
        UpdateStatUI();
    }
}
