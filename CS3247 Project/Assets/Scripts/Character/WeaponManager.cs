using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponManager : MonoBehaviour
{
    [HideInInspector]
    public Faction faction;

    public Transform leftHand;
    public Transform rightHand;
    public Transform aimTarget;

    public WeaponCombination[] weaponCombinations;
    private int weaponIndex = -1;

    private Animator animator;
    private CharacterAction action;
    private CharacterDamageReceiver damageReceiver;

    private Collider ownerCollider;
    private Weapon currentWeapon;
    private Weapon secondaryWeapon;
    private Blocker currentBlocker;

    private void Start()
    {
        animator = GetComponentInChildren<Animator>();
        action = GetComponentInChildren<CharacterAction>();
        damageReceiver = GetComponent<CharacterDamageReceiver>();
        ownerCollider = GetComponent<Collider>();
        SwapWeapon();
    }

    public void SwapWeapon()
    {
        weaponIndex++;
        if (weaponIndex >= weaponCombinations.Length)
        {
            weaponIndex = 0;
        }

        EquipWeapon(weaponCombinations[weaponIndex]);
    }

    private void EquipWeapon(WeaponCombination combination)
    {
        Weapon mainWeapon = Instantiate(combination.mainWeapon).GetComponent<Weapon>();
        EquipMainWeapon(mainWeapon);
        if (combination.secondaryWeapon != null)
        {
            Weapon secondaryWeapon = Instantiate(combination.secondaryWeapon).GetComponent<Weapon>();
            EquipSecondaryWeapon(secondaryWeapon);
        }
        else
        {
            EquipSecondaryWeapon(null);
        }
    }

    private void EquipMainWeapon(Weapon weapon)
    {
        if (weapon == null) return;

        if (currentWeapon != null)
        {
            Destroy(currentWeapon.gameObject);
        }

        action.combatStyle = weapon.combatStyle;
        animator.runtimeAnimatorController = weapon.controller;

        currentWeapon = weapon;

        weapon.SetOwner(this);
    }

    private void EquipSecondaryWeapon(Weapon weapon)
    {
        if (secondaryWeapon != null)
        {
            Destroy(secondaryWeapon.gameObject);
        }

        if (weapon == null) return;

        weapon.SetOwner(this);
        secondaryWeapon = weapon;
    }

    public void EquipBlocker(Blocker blocker)
    {
        if (blocker == null) return;

        if (currentBlocker != null)
        {
            Destroy(currentBlocker.gameObject);
        }

        currentBlocker = blocker;
        currentBlocker.SetOwner(this);
        damageReceiver.blocker = currentBlocker;
    }

    public void SetUpAttack()
    {
        currentWeapon.SetUpAttack();
    }

    public void ActivateWeapon(AttackInfo attackInfo)
    {
        currentWeapon.Activate(attackInfo);
    }

    public Collider GetOwnerCollider()
    {
        return ownerCollider;
    }

    public void CancelAttack()
    {
        currentWeapon.CancelAttack();
    }
}
