using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterStats : MonoBehaviour
{
    // todo: add character stats
    public int health;
    public int maxHealth;
    public Slider healthSlider;

    public Stat attack;
    public Stat defence;

    private void Start()
    {
        health = maxHealth;

        if (healthSlider != null)
        {
            healthSlider.maxValue = maxHealth;
            healthSlider.value = health;
        }
    }

    public virtual void TakeDamage(int damage) {
        damage -= defence.GetValue();
        damage = Mathf.Clamp(damage, 10, int.MaxValue);
        health = Mathf.Clamp(health - damage, 0, maxHealth);

        if (healthSlider != null)
        {
            healthSlider.value = health;
        }

        if (health <= 0 && healthSlider != null)
        {
            healthSlider.gameObject.SetActive(false);
        }
    }
 }
