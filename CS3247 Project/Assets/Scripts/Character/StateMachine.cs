﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Object = System.Object;

// Notes
// 1. What a finite state machine is
// 2. Examples where you'd use one
//     AI, Animation, Game State
// 3. Parts of a State Machine
//     States & Transitions
// 4. States - 3 Parts
//     Tick - Why it's not Update()
//     OnEnter / OnExit (setup & cleanup)
// 5. Transitions
//     Separated from states so they can be re-used
//     Easy transitions from any state

public class StateMachine
{
    public IState _currentState;

    private Dictionary<Type, List<Transition>> _transitions = new Dictionary<Type, List<Transition>>();
    private List<Transition> _currentTransitions = new List<Transition>();
    private List<Transition> _anyTransitions = new List<Transition>();

    private static List<Transition> EmptyTransitions = new List<Transition>(0);

    public void Tick()
    {
        _currentState?.Tick();

        var transition = GetTransition();
        if (transition != null)
        {
            if (transition.To == _currentState && !transition.CanTransitToSelf)
            {
                return;
            }

            SetState(transition.To);
        }
    }

    public void SetState(IState state)
    {
        _currentState?.OnExit();
        _currentState = state;

        _transitions.TryGetValue(_currentState.GetType(), out _currentTransitions);
        if (_currentTransitions == null)
            _currentTransitions = EmptyTransitions;

        _currentState.OnEnter();
    }

    public void AddTransition(IState from, IState to, Func<bool> predicate)
    {
        if (_transitions.TryGetValue(from.GetType(), out var transitions) == false)
        {
            transitions = new List<Transition>();
            _transitions[from.GetType()] = transitions;
        }

        bool canTransitToSelf = from == to;

        transitions.Add(new Transition(to, predicate, canTransitToSelf));
    }

    public void AddAnyTransition(IState state, Func<bool> predicate, bool canTransitToSelf=false)
    {
        _anyTransitions.Add(new Transition(state, predicate, canTransitToSelf));
    }

    private class Transition
    {
        public Func<bool> Condition { get; }
        public IState To { get; }
        public bool CanTransitToSelf { get; }

        public Transition(IState to, Func<bool> condition, bool canTransitToSelf)
        {
            To = to;
            Condition = condition;
            CanTransitToSelf = canTransitToSelf;
        }
    }

    private Transition GetTransition()
    {
        foreach (var transition in _anyTransitions)
            if (transition.Condition())
                return transition;

        foreach (var transition in _currentTransitions)
            if (transition.Condition())
                return transition;

        return null;
    }
}
