using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetDetector : MonoBehaviour
{
    [HideInInspector]
    protected Faction targetFaction;

    public void SetFaction(Faction faction)
    {
        if (faction == Faction.Player)
        {
            targetFaction = Faction.Enemy;
        }
        else
        {
            targetFaction = Faction.Player;
        }
    }
}
