using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public abstract class CharacterManager : MonoBehaviour
{
    public Faction faction;

    public void Init()
    {
        GetComponent<CharacterDamageReceiver>().faction = faction;
        GetComponent<TargetDetector>().SetFaction(faction);
        GetComponent<WeaponManager>().faction = faction;
        GetComponent<CharacterAiming>().faction = faction;
        LevelManager.instance.AddCharacter(transform, faction);
    }

    public virtual void DisableCharacter()
    {
        GetComponent<Collider>().enabled = false;
        GetComponent<Rigidbody>().useGravity = false;
        GetComponentInChildren<CharacterBlocker>().Disable();
        GetComponent<CharacterMotion>().enabled = false;
        NavMeshAgent nav = GetComponent<NavMeshAgent>();
        if (nav != null)
        {
            nav.enabled = false;
        }
        LevelManager.instance.RemoveTarget(transform, faction);
        transform.tag = "Dead";
        enabled = false;
    }
}
