using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetActions : StateMachineBehaviour
{
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.GetComponent<CharacterAction>().ResetAction();
        animator.GetComponentInParent<CharacterMotion>().ResetMotion();
    }
}
