using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterBlocker : MonoBehaviour
{
    public Collider characterCollider;

    private void Start()
    {
        Physics.IgnoreCollision(characterCollider, GetComponent<Collider>());
    }

    public void Disable()
    {
        GetComponent<Collider>().enabled = false;
    }
}