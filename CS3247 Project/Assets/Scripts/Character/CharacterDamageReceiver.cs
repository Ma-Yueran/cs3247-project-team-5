using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterDamageReceiver : DamageReceiver
{
    private CharacterStats stats;

    private const int smallImpact = 0;
    private const int largeImpact = 1;

    public int toughness;
    public int recoverRate;

    public AudioClip[] hitSounds;

    [HideInInspector]
    public bool isBlocking = false;
    [HideInInspector]
    public Blocker blocker;

    private Animator animator;
    private AudioSource source;
    private CharacterAction action;
    private CharacterMotion motion;

    private int currentToughness;
    private bool isHit = false;
    private bool isInterupted = false;

    private bool isInvincible = false;

    private void Start()
    {
        stats = GetComponent<CharacterStats>();
        animator = GetComponentInChildren<Animator>();
        source = gameObject.AddComponent<AudioSource>();
        action = GetComponentInChildren<CharacterAction>();
        motion = GetComponent<CharacterMotion>();
        source.spatialBlend = 1;

        currentToughness = toughness;
        InvokeRepeating("Recover", 0, 1);
        isHit = false;
    }

    public override void TakeDamage(AttackInfo attackInfo, int weaponDamage)
    {
        if (isInvincible)
        {
            return;
        }

        // Check is blocking
        if (isBlocking && blocker != null)
        {
            blocker.PlayBlockSound(attackInfo.power);
            animator.SetTrigger("Blocked Hit");
            motion.DisableCanRotate();
            motion.DisableCanMove();
            return;
        }

        // Hit sound
        source.PlayOneShot(hitSounds[(int)attackInfo.power]);

        // Hit flag
        StartCoroutine(HitForOneSecond());

        // Damage calculation
        float critCheck = Random.value;
        if (critCheck <= 0.1f) { // Add flat 10% crit rate
            weaponDamage = (int) (weaponDamage * 1.5);
        }

        stats.TakeDamage(weaponDamage);

        // Check if player has died
        if (stats.health <= 0)
        {
            Die();
        }

        currentToughness -= attackInfo.strength;

        // Interupted
        if (currentToughness < 0)
        {
            // Interupted flag
            StartCoroutine(InteruptedForOneSecond());

            int impact = smallImpact;
            if (currentToughness + toughness < 0)
            {
                impact = largeImpact;
            }

            // Play Hit Animation
            animator.SetInteger("Hit Direction", (int)attackInfo.attackDirection);
            animator.SetInteger("Hit Impact", impact);
            animator.SetTrigger("Hit");
            action.CancelActions();
            action.DisableAction();

            // Reset toughness
            currentToughness = toughness;
        }
    }

    public void SetInvincible(float time)
    {
        StartCoroutine(SetInvincibleFor(time));
    }

    private IEnumerator SetInvincibleFor(float time)
    {
        isInvincible = true;
        yield return new WaitForSeconds(time);
        isInvincible = false;
    }

    private void Die()
    {
        action.DisableAction();
        animator.SetTrigger("Death");
        motion.DisableCanMove();
        GetComponent<CharacterManager>().DisableCharacter();

        //Adds gold if enemy character dies
        if (faction == Faction.Enemy)
        {
            int goldDrop = stats.attack.GetValue() + stats.defence.GetValue() + (int) Random.Range(10.0f, 50.0f);
            GoldManager.Instance.OnGoldChange(goldDrop);
        }
    }

    private void Recover()
    {
        if (currentToughness < toughness)
        {
            currentToughness += recoverRate;
        }
    }

    public bool IsHit()
    {
        if (isHit)
        {
            isHit = false;
            return true;
        }
        return false;
    }

    private IEnumerator HitForOneSecond()
    {
        isHit = true;
        yield return new WaitForSeconds(1);
        isHit = false;
    }

    public bool IsInterupted()
    {
        if (isInterupted)
        {
            isInterupted = false;
            return true;
        }
        return false;
    }

    private IEnumerator InteruptedForOneSecond()
    {
        isInterupted = true;
        yield return new WaitForSeconds(1);
        isInterupted = false;
    }
}
