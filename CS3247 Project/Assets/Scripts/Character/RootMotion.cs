using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RootMotion : MonoBehaviour
{
    public Transform characterTransform;
    public float speed = 1;

    private Animator animator;

    private void Awake()
    {
        animator = GetComponent<Animator>();
    }

    private void OnAnimatorMove()
    {
        characterTransform.Rotate(animator.deltaRotation.eulerAngles);
        characterTransform.position += speed * animator.deltaPosition;
    }
}
