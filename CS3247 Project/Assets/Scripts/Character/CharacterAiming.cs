using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations.Rigging;

public abstract class CharacterAiming : MonoBehaviour
{
    public Transform target;
    public Transform spineRotation;
    public Rig rig;
    public LayerMask layerMask;
    [HideInInspector]
    public Faction faction;

    public abstract void Aim();

    public abstract void StopAiming();
}
