using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AttackDirection
{
    Front = 0,
    Left = 1,
    Right = 2,
    Back = 3
}

/// <summary>
/// Play different sound / visual effects based on the attack power
/// </summary>
public enum AttackPower
{
    Light = 0,
    Medium = 1,
    Heavy = 2
}

[CreateAssetMenu(fileName = "new attackInfo", menuName = "ScriptableObjects/AttackInfo")]
public class AttackInfo : ScriptableObject
{
    public AttackDirection attackDirection;
    public int strength;
    public AttackPower power;
}
