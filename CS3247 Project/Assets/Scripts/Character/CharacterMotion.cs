using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMotion : MonoBehaviour
{
    public float rotateSpeed = 10;
    public float walkSpeed = 3;
    public float runSpeed = 6;
    protected bool canMove = true;
    protected bool canRotate = true;
    protected bool lookAt = false;
    protected bool isLockOn = false;
    protected Transform lookAtTarget;
    public Transform currentLockOn;

    public virtual void ResetMotion()
    {
        canMove = true;
        canRotate = true;
        lookAt = false;
    }

    public virtual void DisableCanMove()
    {
        canMove = false;
    }

    public void DisableCanRotate()
    {
        canRotate = false;
    }

    public virtual void LookAtTarget(Transform target)
    {
        if (target == null)
        {
            return;
        }

        lookAt = true;
        canRotate = false;
        lookAtTarget = target;
    }

    public void SetLockOn(Transform target)
    {
        if (target == null)
        {
            return;
        }

        currentLockOn = target;
        isLockOn = true;
    }

    public void StopLockOn()
    {
        currentLockOn = null;
        isLockOn = false;
    }

    public virtual void ResetTargets()
    {
        lookAtTarget = null;
        currentLockOn = null;
        isLockOn = false;
        lookAt = false;
    }
}
