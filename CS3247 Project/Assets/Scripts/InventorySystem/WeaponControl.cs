using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class WeaponControl : MonoBehaviour
{
    public Button Sword;
    public Button Arrow;
    public Button Magic;
    Inventory inventory;    // Our current inventory
    private Color whiteColor = new Color(1.0f, 1.0f, 1.0f);
    void Start()
    {
        inventory = Inventory.instance;
        // activateSword();
    }
    public void activateSword()
    {
        inventory.AccessItems("sword");
        whiteBackground();
        Sword.image.color = new Color(23f / 255, 40f / 255, 255f / 255);
        inventory.onItemChangedCallback.Invoke();
    }
    public void activateArrow()
    {
        inventory.AccessItems("bow");
        whiteBackground();
        Arrow.image.color = new Color(23f / 255, 40f / 255, 255f / 255);
        inventory.onItemChangedCallback.Invoke();
    }
    public void activateArmor()
    {
        inventory.AccessItems("armor");
        whiteBackground();
        Magic.image.color = new Color(23f / 255, 40f / 255, 255f / 255);
        inventory.onItemChangedCallback.Invoke();
    }
    void whiteBackground()
    {
        Sword.image.color = whiteColor;
        Arrow.image.color = whiteColor;
        Magic.image.color = whiteColor;
    }
    public void activeWeapons()
    {
        Sword.gameObject.SetActive(true);
        Arrow.gameObject.SetActive(true);
        Magic.gameObject.SetActive(true);
        activateSword();
    }
    public void activeItems()
    {
        Sword.gameObject.SetActive(false);
        Arrow.gameObject.SetActive(false);
        Magic.gameObject.SetActive(false);
        inventory.AccessItems("consuming");
    }
}
