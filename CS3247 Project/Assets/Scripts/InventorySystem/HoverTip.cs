using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class HoverTip : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField] private string description;
    public void OnPointerEnter(PointerEventData eventData)
    {
        ToolTip.ShowTooltip_Static(description);
    }
    public void OnPointerExit(PointerEventData eventData)
    {
        ToolTip.HideTooltip_Static();
    }
}
