using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponPage : MonoBehaviour
{
    public GameObject weapon_page;
    public GameObject item_page;
    
    public void ShowPage()
    {
        weapon_page.SetActive(true);
        item_page.SetActive(false);
    }
}
