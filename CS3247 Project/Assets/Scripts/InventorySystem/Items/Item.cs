using UnityEngine;

/* The base item class. All items should derive from this. */

[CreateAssetMenu(fileName = "New Item", menuName = "Inventory/Item")]
public class Item : ScriptableObject
{

    new public string name = "New Item";    // Name of the item
    public Sprite icon = null;              // Item icon
    public string attribute = "sword";
    public bool showInInventory = true;
    public float attackIncrease = 0.0f;
    public float attackSpeedBuff = 1.0f;
    public float healthRestore = 0.0f;

    public int Attack;
    public int Defence;
    public int Cost;
    public EquipmentType Type;

    // Called when the item is pressed in the inventory
    public virtual void Use()
    {
        Inventory.instance.UsingItem(this);
        Debug.Log("Using " + name);
        // Use the item
        // Something may happen
    }

    // Call this method to remove the item from inventory
    public void RemoveFromInventory()
    {
        Inventory.instance.Remove(this);
    }

    public virtual string ItemInfo()
    {
        string info= "";
        if (attribute == "consuming")
            info += "\nHealth Restore: " + healthRestore.ToString();
        else
        {
            string attackSign = Attack < 0 ? "" : "+";
            string defenceSign = Defence < 0 ? "" : "+";
            info += attackSign + " " + Attack.ToString() + " Attack\n";
            info += defenceSign + " " + Defence.ToString() + " Defence";
        }
        return info;
    }
}
