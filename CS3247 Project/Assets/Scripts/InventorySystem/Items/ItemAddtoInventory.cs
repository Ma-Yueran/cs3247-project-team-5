using UnityEngine;

public class ItemAddtoInventory : MonoBehaviour
{
    public Item item;
    // Start is called before the first frame update
    void Start()
    {
        addItem();
    }
    void addItem()
    {
        transform.position = new Vector3(0.0f, 0.0f, -10.0f);
        if (item.showInInventory)
            Inventory.instance.AddItems(item, item.attribute);
    }
}
