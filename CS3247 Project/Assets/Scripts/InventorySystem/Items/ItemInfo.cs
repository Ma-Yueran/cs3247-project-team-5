using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ItemInfo : MonoBehaviour
{
    public TMP_Text itemName;
    public TMP_Text info;

    public void SetInfo(string name, string info)
    {
        this.itemName.text = name;
        this.info.text = info;
    }
}
