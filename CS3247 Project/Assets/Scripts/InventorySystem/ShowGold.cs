using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowGold : MonoBehaviour
{
    [SerializeField] int money = 99502;

    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Text>().text = money.ToString();
    }

    // Update is called once per frame
    void Update()
    {

    }
}
