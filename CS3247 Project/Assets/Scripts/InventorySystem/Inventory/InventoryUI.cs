using UnityEngine;

/* This object manages the inventory UI. */

public class InventoryUI : UIObject
{
    public static InventoryUI Instance;

    public Transform itemsParent;   // The parent object of all the items

    Inventory inventory;    // Our current inventory

    public Transform canvas;
    public GameObject itemInfoPrefab;
    private GameObject currentItemInfo = null;


    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(this);
        } else
        {
            Instance = this;
        }
    }

    private void Start()
    {
        Init();

        inventory = Inventory.instance;
        inventory.onItemChangedCallback += UpdateUI;    // Subscribe to the onItemChanged callback

        inventory.onItemChangedCallback.Invoke();
    }

    // Update the inventory UI by:
    //		- Adding items
    //		- Clearing empty slots
    // This is called using a delegate on the Inventory.
    public void UpdateUI()
    {
        InventorySlot[] slots = itemsParent.GetComponentsInChildren<InventorySlot>();
        for (int i = 0; i < slots.Length; i++)
        {
            if (i < inventory.items.Count)
            {
                slots[i].AddItem(inventory.items[i]);
            }
            else
            {
                slots[i].ClearSlot();
            }
        }
        PlayerStats.Instance.UpdateStatUI();
    }

    public void DisplayItemInfo(string itemName, string itemDescriptor, Vector2 buttonPos)
    {
        DestroyItemInfo();

        buttonPos.x += -210f;

        currentItemInfo = Instantiate(itemInfoPrefab, buttonPos, Quaternion.identity, canvas);
        currentItemInfo.GetComponent<ItemInfo>().SetInfo(itemName, itemDescriptor);
    }

    public void DestroyItemInfo()
    {
        if (currentItemInfo != null)
        {
            Destroy(currentItemInfo);
        }
    }
}
