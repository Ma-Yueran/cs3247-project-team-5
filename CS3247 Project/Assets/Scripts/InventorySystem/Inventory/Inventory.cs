using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Inventory : MonoBehaviour
{
    #region Singleton

    public static Inventory instance;

    void Awake()
    {
        instance = this;
    }

    #endregion

    public InventoryUI inventoryUI;

    public delegate void OnItemChanged();
    public OnItemChanged onItemChangedCallback;

    public int space = 15;  // Amount of item spaces

    public EquipmentSlot equippedSword;
    public EquipmentSlot equippedBow;
    public EquipmentSlot equippedArmor;

    // Our current list of items in the inventory
    public List<Item> items = new List<Item>();
    public List<Item> swordItems = new List<Item>();
    public List<Item> bowItems = new List<Item>();
    public List<Item> armorItems = new List<Item>();
    public List<Item> comsumingItems = new List<Item>();

    public WeaponControl weaponControl;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.I))
        {
            if (inventoryUI.IsEnabled())
            {
                inventoryUI.DisableUI();
            }
            else
            {
                inventoryUI.EnableUI();
            }
            inventoryUI.UpdateUI();
        }
    }

    // Add a new item if enough room
    public void Add(Item item)
    {
        // Don't do anything if it's a default item
        if (item.showInInventory)
        {
            // Check if out of space
            if (items.Count >= space)
            {
                Debug.Log("Not enough room.");
            }

            items.Add(item);    // Add item to list

            // Trigger callback
            if (onItemChangedCallback != null)
                onItemChangedCallback.Invoke();

        }
    }

    public void AddItems(Item item, string type)
    {
        // Don't do anything if it's a default item
        if (item.showInInventory)
        {
            // Check if out of space
            if (swordItems.Count >= space)
            {
                Debug.Log("Not enough room.");
            }

            switch (type)
            {
                case "sword":
                    swordItems.Add(item);    // Add item to list
                    AccessItems("sword");
                    break;

                case "bow":
                    bowItems.Add(item);    // Add item to list
                    AccessItems("bow");
                    break;

                case "armor":
                    armorItems.Add(item);    // Add item to list
                    AccessItems("armor");
                    break;

                case "consuming":
                    comsumingItems.Add(item);    // Add item to list
                    AccessItems("consuming");
                    break;
            }
        }
    }
    public void AccessItems(string type)
    {
        switch (type)
        {
            case "sword":
                items = swordItems;
                break;

            case "bow":
                items = bowItems;
                break;

            case "armor":
                items = armorItems;
                break;

            case "consuming":
                items = comsumingItems;
                break;
        }
        // Trigger callback
        if (onItemChangedCallback != null)
            onItemChangedCallback.Invoke();
    }

    // Remove an item
    public void Remove(Item item)
    {
        items.Remove(item);

        switch (item.attribute)
        {
            case "sword":
                swordItems.Remove(item);
                break;

            case "bow":
                swordItems.Remove(item);
                break;

            case "armor":
                swordItems.Remove(item);
                break;

            case "consuming":
                swordItems.Remove(item);
                break;
        }

        if (onItemChangedCallback != null)
            onItemChangedCallback.Invoke();
    }

    public void Clear()
    {
        foreach (Item item in items)
            item.showInInventory = false;
        items.Clear();
        onItemChangedCallback.Invoke();
    }

    public void UsingItem(Item item)
    {
        if (item.attribute == "consuming")
            Remove(item);
        else
        {
            EquipItem(item);
        }
    }

    private void EquipItem(Item item)
    {
        Item currItem = null;
        switch (item.Type)
        {
            case EquipmentType.Sword:
                if (equippedSword.item != null)
                {
                    currItem = equippedSword.item;
                    //Add(equippedSword);
                    AddItems(equippedSword.item, equippedSword.item.attribute);
                }
                equippedSword.AddItem(item);
                break;

            case EquipmentType.Bow:
                if (equippedBow.item != null)
                {
                    currItem = equippedBow.item;
                    //Add(equippedBow);
                    AddItems(equippedBow.item, equippedBow.item.attribute);
                }
                equippedBow.AddItem(item);
                break;

            case EquipmentType.Armor:
                if (equippedArmor.item != null)
                {
                    currItem = equippedArmor.item;
                    //Add(equippedArmor);
                    AddItems(equippedArmor.item, equippedArmor.item.attribute);
                }
                equippedArmor.AddItem(item);
                break;
        }
        Remove(item);
        PlayerStats.Instance.ChangeEquipment(currItem, item);
        InventoryUI.Instance.DestroyItemInfo();
    }

    public void Unequip(int index)
    {
        EquipmentSlot slot = null;
        switch (index)
        {
            case 0:
                
                slot = equippedSword;
                weaponControl.activateSword();
                break;

            case 1:
                slot = equippedBow;
                weaponControl.activateArrow();
                break;

            case 2:
                slot = equippedArmor;
                weaponControl.activateArmor();
                break;
        }

        if (slot.item == null)
        {
            return;
        }

        AddItems(slot.item, slot.item.attribute);
        PlayerStats.Instance.Unequip(slot.item);
        InventoryUI.Instance.DestroyItemInfo();
        slot.ClearSlot();
    }
}
