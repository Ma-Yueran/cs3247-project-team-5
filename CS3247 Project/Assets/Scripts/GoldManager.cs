using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GoldManager : MonoBehaviour
{
    public static GoldManager Instance;

    public int gold;
    public TMP_Text GoldText;

    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(this);
        } else
        {
            Instance = this;
            gold = 100;
            GoldText.text = gold.ToString();
        }
    }

    public void OnGoldChange(int goldAmount)
    {
        gold += goldAmount;
        GoldText.text = gold.ToString();
        PlayerStats.Instance.UpdateStatUI();
    }

}
