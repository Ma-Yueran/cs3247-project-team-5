using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New CameraSetting", menuName = "ScriptableObjects/CameraSetting")]
public class CameraSetting : ScriptableObject
{
    public Vector3 targetLocalPosition;
    public float distanceToCamera = 3.5f;
}
