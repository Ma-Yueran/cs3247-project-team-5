using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstantiateEnemy : MonoBehaviour
{
    [SerializeField]
    private GameObject enemy;

    [SerializeField]
    private float spawnInterval = 5.0f;

    [SerializeField]
    private int maxSpawns = 5;

    private float timer = 0;
    private int spawnCounter = 0;

    public void instantiateHere()
    {
        //Instantiate(enemy, spawnTransform);
        CharacterManager character = Instantiate(enemy, transform.position, Quaternion.identity).GetComponent<CharacterManager>();
        character.faction = Faction.Enemy;
        character.Init();
        spawnCounter++;
    }

    // Update is called once per frame
    private void Update()
    {
        timer += Time.deltaTime;
        if (timer >= spawnInterval && spawnCounter < maxSpawns)
        {
            instantiateHere();
            timer = 0;
        }
    }
}
