using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class AllySlot : MonoBehaviour
{
    public Image image;
    public TextMeshProUGUI text;
    public Slider cdSlider;
    public float cd = 3;

    private float cdTimer = 0;

    public void SetUp(Sprite avatar, int cost, float cd)
    {
        gameObject.SetActive(true);
        image.sprite = avatar;
        text.text = cost + "";
        cdSlider.value = 0;
        cdTimer = 0;
        this.cd = cd;
    }

    public bool CanSpawn()
    {
        return cdTimer == 0;
    }

    public void ResetCD()
    {
        cdTimer = cd;
    }

    private void Update()
    {
        if (cdTimer > 0)
        {
            cdTimer -= Time.deltaTime;
            cdSlider.value = cdTimer / cd;
        }
        else
        {
            cdTimer = 0;
        }
    }
}
