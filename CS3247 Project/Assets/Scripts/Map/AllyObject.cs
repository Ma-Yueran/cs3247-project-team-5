using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "New Ally Object", menuName = "ScriptableObjects/AllyObject")]
public class AllyObject : ScriptableObject
{
    public GameObject allyPrefab;
    public int cost;
    public Sprite avatar;
    public float cd;
}
