using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseGame : MonoBehaviour
{
    [SerializeField]
    private UIObject pauseMenu;

    [SerializeField]
    private UIObject controlMenu;

    private void Update()
    {
        if (Input.GetKeyDown("p"))
        {
            if (pauseMenu.IsEnabled())
            {
                pauseMenu.DisableUI();
            }
            else
            {
                pauseMenu.EnableUI();
            }
        }
        if (Input.GetKeyDown("c"))
        {
            if (controlMenu.IsEnabled())
            {
                controlMenu.DisableUI();
            }
            else
            {
                controlMenu.EnableUI();
            }
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (controlMenu.IsEnabled())
            {
                controlMenu.DisableUI();
            }
        }
    }
}
