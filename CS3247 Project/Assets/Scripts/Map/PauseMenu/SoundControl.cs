using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundControl : MonoBehaviour
{
    public Slider soundSlider;

    // Update is called once per frame
    private void Update()
    {
        AudioListener.volume = soundSlider.value;
    }
}
