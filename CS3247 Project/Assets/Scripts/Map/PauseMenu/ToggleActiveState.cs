using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleActiveState : MonoBehaviour
{
    [SerializeField]
    private GameObject objectToBeToggled;

    private Button button;

    private void Start()
    {
        button = GetComponent<Button>();
        button.onClick.AddListener(toggleActiveState);
    }

    public void toggleActiveState()
    {
        objectToBeToggled.SetActive(!objectToBeToggled.activeSelf);
    }
}
