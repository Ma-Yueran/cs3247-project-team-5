using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class AllyInstantiation : MonoBehaviour
{
    public AllyObject[] allies;
    public Transform spawnPoint;

    public int maxResource;
    public int resourceIncreaseSpeed;
    private int currentResource = 0;

    public AllySlot[] allySlots;
    public TextMeshProUGUI resourceText;

    private void Start()
    {
        InvokeRepeating("GainResource", 0, 1);
        SetupUI();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            InstantiateAlly(0);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            InstantiateAlly(1);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            InstantiateAlly(2);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            InstantiateAlly(3);
        }
    }

    private void SetupUI()
    {
        for (int i = 0; i < allies.Length; i++)
        {
            allySlots[i].SetUp(allies[i].avatar, allies[i].cost, allies[i].cd);
        }
    }

    private void GainResource()
    {
        currentResource += resourceIncreaseSpeed;
        if (currentResource > maxResource)
        {
            currentResource = maxResource;
        }

        resourceText.text = currentResource + "/" + maxResource;
    }

    public void InstantiateAlly(int index)
    {
        if (index >= allies.Length)
        {
            return;
        }

        if (currentResource < allies[index].cost)
        {
            return;
        }

        if (!allySlots[index].CanSpawn())
        {
            return;
        }

        allySlots[index].ResetCD();

        currentResource -= allies[index].cost;

        float randomX = Random.Range(-1.0f, 1.0f);
        if (randomX >= 0) randomX += 1.0f;
        else randomX -= 1.0f;
        float randomZ = Random.Range(-1.0f, 1.0f);
        if (randomZ >= 0) randomZ += 1.0f;
        else randomZ -= 1.0f;

        Vector3 randomOffset = new Vector3(randomX, 0.5f, randomZ);

        CharacterManager character = Instantiate(allies[index].allyPrefab, spawnPoint.position + randomOffset, spawnPoint.rotation).GetComponent<CharacterManager>();
        character.faction = Faction.Player;
        character.Init();
    }
}
