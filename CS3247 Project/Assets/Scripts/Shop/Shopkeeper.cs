using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Shopkeeper : MonoBehaviour
{
    [SerializeField]
    private GameObject InteractUI;

    [SerializeField]
    private UIObject ShopUI;

    private bool canInteract = false;

    private void OnTriggerEnter(Collider other)
    {
        PlayerManager manager = other.GetComponent<PlayerManager>();
        if (manager == null)
        {
            return;
        }
        InteractUI.SetActive(true);
        canInteract = true;
    }

    private void OnTriggerExit(Collider other)
    {
        PlayerManager manager = other.GetComponent<PlayerManager>();
        if (manager == null)
        {
            return;
        }
        InteractUI.SetActive(false);
        canInteract = false;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.F) && canInteract)
        {
            if (ShopUI.IsEnabled())
            {
                ShopUI.DisableUI();
            } 
            else
            {
                ShopUI.EnableUI();
            }
        }
    }
}
