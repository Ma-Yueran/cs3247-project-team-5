using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
public class Buy : MonoBehaviour
{
    public Text Gold;
    public Text Price;

    public void SetText()
    {
        Debug.Log(Convert.ToInt32(Gold.text).ToString());
        Debug.Log(Convert.ToInt32(Price.text).ToString());
        int value = Convert.ToInt32(Gold.text) - Convert.ToInt32(Price.text);
        if (value > 0)
            Gold.text = value.ToString("000000");
    }
}
