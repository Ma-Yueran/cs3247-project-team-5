using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shop : MonoBehaviour
{
    public LootMaterial[] ShopItems;
    
    // Start is called before the first frame update
    void Start()
    {
        // Initialize shop
    }

    LootMaterial SelectItem(int index) {
        return ShopItems[index];
    }

    void BuyItem(PlayerData playerData, LootMaterial item, int quantity)
    {
        if (playerData.gold < (item.price * quantity)) {
            return;
        }

        playerData.gold -= (item.price * quantity);
        playerData.materialInventory.Add(item, quantity);

    }

    void SellItem(PlayerData playerData, LootMaterial item, int quantity)
    {
        int holdingQuantity = playerData.materialInventory.GetQuantity(item);

        // If player tries to sell more than what they have
        if (holdingQuantity < quantity) {
            quantity = holdingQuantity;
        }

        playerData.materialInventory.Destroy(item, quantity);

        playerData.gold += (item.refundPrice * quantity);

    }
}
