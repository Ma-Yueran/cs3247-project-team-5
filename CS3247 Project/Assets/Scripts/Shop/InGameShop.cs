using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class InGameShop : UIObject
{
    public Item[] SwordList;
    public Item[] BowList;
    public Item[] ArmorList;

    public EquipmentType selectedTab = EquipmentType.Sword;
    public Item selectedEquipment;

    [Header("UI Text Components")]
    public TMP_Text Name;
    public TMP_Text AttackStat;
    public TMP_Text DefenceStat;
    public TMP_Text Cost;
    public TMP_Text Gold;

    [Header("UI Components")]
    public Button BuyButton;
    public GameObject NotEnoughGoldWarning;
    public ShopSlot[] ShopSlots;
    public ShopSlot SelectedEquipmentIcon;

    private void Start()
    {
        Init();
        UpdateUI();
    }

    public void OnClickTab(int index)
    {
        selectedTab = (EquipmentType)index;
        UpdateUI();
    }

    public void OnClickShopSlot(int index)
    {
        Item[] items = GetItems();
        selectedEquipment = items[index];
        UpdateUI();
    }

    private Item[] GetItems()
    {
        switch (selectedTab)
        {
            case EquipmentType.Sword:
                return SwordList;

            case EquipmentType.Bow:
                return BowList;

            case EquipmentType.Armor:
                return ArmorList;
        }
        return null;
    }

    public void Buy()
    {
        if (selectedEquipment.Cost > GoldManager.Instance.gold)
        {
            NotEnoughGoldWarning.SetActive(true);
            return;
        }
        GoldManager.Instance.OnGoldChange(-selectedEquipment.Cost);
        UpdateUI();
        Inventory.instance.AddItems(selectedEquipment, selectedEquipment.attribute);
    }

    private void UpdateShopSlots()
    {
        for (int i = 0; i < ShopSlots.Length; i++)
        {
            Item[] items = GetItems();
            if (i < items.Length)
            {
                ShopSlots[i].AddItem(items[i]);
            } else
            {
                ShopSlots[i].ClearSlot();
            }
        }
    }

    private void UpdateUI()
    {
        Gold.text = GoldManager.Instance.gold.ToString();

        //Updates Shop Icons
        UpdateShopSlots();

        // Updates information tab
        if (selectedEquipment != null)
        {
            BuyButton.enabled = true;
            Name.text = selectedEquipment.name;
            AttackStat.text = selectedEquipment.Attack.ToString();
            DefenceStat.text = selectedEquipment.Defence.ToString();
            Cost.text = selectedEquipment.Cost.ToString();
            SelectedEquipmentIcon.AddItem(selectedEquipment);
        } else
        {
            BuyButton.enabled = false;
        }
    }
}
