using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weaponsmith : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void enhanceWeapon(Weapon weapon, EnhancementMaterial[] materials) {
        foreach(EnhancementMaterial material in materials) {
            weapon.weaponEXP += material.enhancementEXP;
            
            // remove from inventory
        }

    }
}
