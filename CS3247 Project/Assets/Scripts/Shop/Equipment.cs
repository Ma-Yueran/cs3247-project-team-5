using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum EquipmentType
{
    Sword,
    Bow,
    Armor
}

public class Equipment
{
    public int Attack;
    public int Defence;
    public int Cost;
    public EquipmentType Type;

    public Equipment(int attack, int defence, int cost, EquipmentType type)
    {
        Attack = attack;
        Defence = defence;
        Cost = cost;
        Type = type;
    }
}
