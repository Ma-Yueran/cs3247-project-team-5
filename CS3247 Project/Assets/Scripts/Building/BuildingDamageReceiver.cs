using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingDamageReceiver : DamageReceiver
{
    public AudioClip hitSound;

    private BuildingStats stats;
    private Animator animator;
    private AudioSource source;

    private void Start()
    {
        stats = GetComponent<BuildingStats>();
        animator = GetComponentInChildren<Animator>();
        source = GetComponent<AudioSource>();
    }

    public override void TakeDamage(AttackInfo attackInfo, int weaponDamage)
    {
        source.PlayOneShot(hitSound);

        stats.TakeDamage(weaponDamage);

        if (stats.currentHealth <= 0)
        {
            Destroyed();
        }
    }

    private void Destroyed()
    {
        animator.SetTrigger("Destroyed");
        GetComponent<BuildingManager>().DisableBuilding();
    }
}
