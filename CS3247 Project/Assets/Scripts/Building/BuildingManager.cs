using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingManager : MonoBehaviour
{
    public Faction faction;
    public Transform[] hitPoints;

    private void Start()
    {
        GetComponent<DamageReceiver>().faction = faction;
        foreach (Transform t in hitPoints)
        {
            LevelManager.instance.AddCharacter(t, faction);
        }
    }

    public virtual void DisableBuilding()
    {
        foreach (Collider collider in GetComponents<Collider>())
        {
            collider.enabled = false;
        }

        foreach (Transform t in hitPoints)
        {
            LevelManager.instance.RemoveTarget(t, faction);
        }
    }
}
