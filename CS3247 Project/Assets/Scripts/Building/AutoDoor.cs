using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoDoor : MonoBehaviour
{
    public Faction faction;

    private Animator animator;
    private int count = 0;

    private void Start()
    {
        animator = GetComponent<Animator>();
    }

    private void Update()
    {
        bool openDoor = count > 0;
        animator.SetBool("Open", openDoor);
    }

    private void OnTriggerEnter(Collider other)
    {
        CharacterManager character = other.GetComponent<CharacterManager>();

        if (character != null && character.faction == faction)
        {
            count++;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        CharacterManager character = other.GetComponent<CharacterManager>();

        if (character != null && character.faction == faction)
        {
            count--;
        }
    }
}
