using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatueManager : BuildingManager
{
    public GameOver gameOverScreen;

    public override void DisableBuilding()
    {
        base.DisableBuilding();
        gameOverScreen.TriggerLoseScreen();
    }
}
