using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Faction
{
    Player,
    Enemy
}

public class LevelManager : MonoBehaviour
{
    public static LevelManager instance;

    public List<Transform> enemies;

    public List<Transform> playerAndAllies;

    private List<Transform> removedCharacters;

    private void Awake()
    {
        instance = this;
        removedCharacters = new List<Transform>();
    }

    public List<Transform> GetTargets(Faction faction)
    {
        if (faction == Faction.Enemy)
        {
            return enemies;
        }
        else
        {
            return playerAndAllies;
        }
    }

    public void AddCharacter(Transform character, Faction faction)
    {
        GetTargets(faction).Add(character);
    }

    public void RemoveTarget(Transform transform, Faction faction)
    {
        GetTargets(faction).Remove(transform);
        removedCharacters.Add(transform);
    }

    public void ClearDisabledCharacters()
    {
        foreach (Transform character in playerAndAllies)
        {
            CharacterMotion motion = character.GetComponent<AIMotion>();
            if (motion != null)
            {
                motion.ResetTargets();
            }
        }

        foreach (Transform removed in removedCharacters)
        {
            Destroy(removed.gameObject, 8f);
        }

        removedCharacters.Clear();
    }
}
