using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObjects/EnemySpawnInfo", fileName = "new EnemySpawnInfo")]
public class EnemySpawnInfo : ScriptableObject
{
    public GameObject enemyPrefab;
    public float spawnRate;
    public float spawnDelay;
    [HideInInspector]
    public float spawnTimer;
}
