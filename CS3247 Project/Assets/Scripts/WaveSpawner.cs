 using UnityEngine;
 using System.Collections;
 using System.Collections.Generic;
 using TMPro;


 
 [System.Serializable]
 public class Wave
 {
    public EnemySpawnInfo[] enemySpawns;
    public float duration;
    public AudioClip music;
 }

 
 public class WaveSpawner : MonoBehaviour
 {
    public AudioClip waveStartAudio;
    public AudioClip defaultMusic;
    public Wave[] waves;
    public int nextWave = 0;

    public Transform[] spawnPoints;

    public float timeBetweenWaves = 5f;
    private float waveCountdown;

    public TextMeshProUGUI waveCountdownTextUI;

    private float waveTimer = 0;
    private bool isWaveOver = false;

    public GameOver gameOverScreen;

    private AudioSource bgm;
    private AudioSource soundEffect;

    private void Awake()
    {
        bgm = gameObject.AddComponent<AudioSource>();
        bgm.clip = defaultMusic;
        bgm.Play();
        bgm.loop = true;
        soundEffect = gameObject.AddComponent<AudioSource>();
    }

    void Start()
    {
        if (spawnPoints.Length == 0)
        {
            Debug.LogError("No spawn points referenced");
        }
        waveCountdown = timeBetweenWaves;
    }

    void Update()
    {
        if (waveTimer > 0)
        {
            SpawnWave(waves[nextWave]);
            waveTimer -= Time.deltaTime;
            isWaveOver = false;
        }
        else
        {
            if (!isEnemyAlive() && !isWaveOver)
            {
                WaveCompleted();
                isWaveOver = true;
            }
        }

        if (isWaveOver)
        {
            waveCountdown -= Time.deltaTime;
            if (waveCountdown > 0)
            {
                waveCountdownTextUI.text = "Time to next wave: " + ((int)waveCountdown).ToString();
            }
            else
            {
                foreach (EnemySpawnInfo spawn in waves[nextWave].enemySpawns)
                {
                    spawn.spawnTimer = spawn.spawnDelay;
                }
                waveTimer = waves[nextWave].duration;
                
                // music
                if (waves[nextWave].music != null)
                {
                    bgm.clip = waves[nextWave].music;
                    bgm.Play();
                }

                soundEffect.PlayOneShot(waveStartAudio);
            }
        }
        
    }

    bool isEnemyAlive()
    {
        return LevelManager.instance.GetTargets(Faction.Enemy).Count != 0;
    }

    private void SpawnWave(Wave wave)
    {
        waveCountdownTextUI.text = "Wave " + (nextWave + 1).ToString();

        foreach (EnemySpawnInfo spawn in wave.enemySpawns)
        {
            spawn.spawnTimer -= Time.deltaTime;
            if (spawn.spawnTimer < 0)
            {
                SpawnEnemy(spawn.enemyPrefab);
                spawn.spawnTimer = spawn.spawnRate;
            }
        }
    }
 
    void SpawnEnemy(GameObject _enemy)
    {
        if (spawnPoints.Length == 0)
        {
            Debug.LogError("No spawn points found");
        }

        Transform _spawnPoint = spawnPoints[ Random.Range(0, spawnPoints.Length) ];
        // Debug.Log("Spawning Enemy: " + _enemy.name + " at spawn point " + _spawnPoint);
        CharacterManager character = Instantiate(_enemy, _spawnPoint.position, Quaternion.identity).GetComponent<CharacterManager>();
        character.faction = Faction.Enemy;
        character.Init();
        
    }

    void WaveCompleted()
    {
        LevelManager.instance.ClearDisabledCharacters();

        waveCountdown = timeBetweenWaves;

        if (nextWave + 1 > waves.Length - 1)
        {
            gameOverScreen.TriggerVictoryScreen();
        }
        else
        {
            nextWave++;
        }        
    }
 }
