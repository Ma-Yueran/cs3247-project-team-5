using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadMain : MonoBehaviour
{
    private Button button;

    [SerializeField]
    private string sceneName;

    private void Start()
    {
        button = GetComponent<Button>();
        button.onClick.AddListener(loadMain);
    }

    private void loadMain()
    {
        LoadScene loadScene = new LoadScene(sceneName);
        loadScene.loadScene();
        Time.timeScale = 1;
    }
}
