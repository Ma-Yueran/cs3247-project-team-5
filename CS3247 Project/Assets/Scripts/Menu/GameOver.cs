using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOver : MonoBehaviour
{
    public UIObject loseScreen;
    public UIObject victoryScreen;

    public void TriggerLoseScreen() {
        loseScreen.EnableUI();
    }

    public void TriggerVictoryScreen()
    {
        victoryScreen.EnableUI();
    }
}
