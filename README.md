# CS3247 Project Team 5

Unity Version: 2020.3.27f1 (Please use the same version to open the project!)

To build:

1. Download the source code / clone the repository

2. Open the project in Unity

3. Build using the Unity Editor (details can be found online)